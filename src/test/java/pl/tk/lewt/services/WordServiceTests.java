package pl.tk.lewt.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.tk.lewt.dao.CourseDao;
import pl.tk.lewt.dao.WordDao;
import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.WordDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.entities.Word;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.tk.lewt.utils.DtoEntityMapper.toDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class WordServiceTests {

    private static String ORIGINAL_WORD = "Sample word";
    private static String TRANSLATED_WORD = "Sample translated word";
    private static List<String> TRANSLATED_WORDS = Arrays.asList(TRANSLATED_WORD);
    @Autowired
    private WordService wordService;

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private WordDao wordDao;

    @Test
    public void shouldCreateNewWord() {
        // given
        final WordDTO wordDTO = new WordDTO();
        wordDTO.setOriginalWord(ORIGINAL_WORD);
        wordDTO.setTranslatedWords(TRANSLATED_WORDS);
        final CourseDTO courseDTO = toDTO(courseDao.findById(1L));
        wordDTO.setCourseDTO(courseDTO);

        // when
        wordService.add(wordDTO);

        // then
        WordDTO newWordDTO = toDTO(wordDao.findByOriginalWord(ORIGINAL_WORD));
        assertThat(newWordDTO).isNotNull();
        assertThat(newWordDTO.getTranslatedWords().size()).isEqualTo(TRANSLATED_WORDS.size());
        assertThat(newWordDTO.getTranslatedWords()).isEqualTo(TRANSLATED_WORDS);
    }
}
