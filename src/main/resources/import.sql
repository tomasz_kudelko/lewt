insert into USER (ID, LOGIN, EMAIL) values (1, 'tk', 'tk@tk.pl');

insert into COURSE (ID, NAME, ACCESSIBILITY) values (1, "English", "PRIVATE");
insert into COURSE (ID, NAME, ACCESSIBILITY) values (2, "German", "PUBLIC");

insert into WORD (ID, ORIGINAL_WORD, TRANSLATED_WORDS, COURSE_ID) values (1, "Original word", "Translated word", 1);