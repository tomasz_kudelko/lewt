package pl.tk.lewt.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.services.CourseService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/courses")
@ResponseBody
public class CourseController {

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/getAllCourses", method = RequestMethod.GET)
    public List<CourseDTO> getAllCourses() {
        final List<CourseDTO> allCourses = courseService.findAll();
        return allCourses;
    }
}
