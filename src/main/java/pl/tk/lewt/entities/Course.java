package pl.tk.lewt.entities;

import pl.tk.lewt.entities.enums.CourseAccessibility;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Course extends BaseEntity{

    @Column(unique = true, nullable = false)
    private String name;

    @Column(name = "ACCESSIBILITY", nullable = false)
    @Enumerated(EnumType.STRING)
    private CourseAccessibility courseAccessibility;

    @OneToMany(mappedBy = "course")
    private Set<Word> wordSet;

    @ManyToMany(mappedBy = "courses")
    private Set<User> enrolledUsers;

    public Course()
    {
        wordSet = new HashSet<>();
        courseAccessibility = CourseAccessibility.PRIVATE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseAccessibility getCourseAccessibility() {
        return courseAccessibility;
    }

    public void setCourseAccessibility(CourseAccessibility courseAccessibility) {
        this.courseAccessibility = courseAccessibility;
    }

    public Set<Word> getWordSet() {
        return wordSet;
    }

    public void setWordSet(Set<Word> wordSet) {
        this.wordSet = wordSet;
    }
}