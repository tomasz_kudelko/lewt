package pl.tk.lewt.entities;

import pl.tk.lewt.utils.StringListConverter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Word extends BaseEntity {

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "COURSE_ID", nullable = false)
    private Course course;

    @Column
    private byte[] image;

    @Column(nullable = false)
    private String originalWord;

    @Column(nullable = false)
    @Convert(converter = StringListConverter.class)
    private List<String> translatedWords;

    public Word() {
        translatedWords = new ArrayList<>();
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getOriginalWord() {
        return originalWord;
    }

    public void setOriginalWord(String originalWord) {
        this.originalWord = originalWord;
    }

    public List<String> getTranslatedWords() {
        return translatedWords;
    }

    public void setTranslatedWords(List<String> translatedWords) {
        this.translatedWords = translatedWords;
    }
}
