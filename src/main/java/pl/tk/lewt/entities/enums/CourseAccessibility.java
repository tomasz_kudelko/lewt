package pl.tk.lewt.entities.enums;

public enum CourseAccessibility {
    PUBLIC, PRIVATE, LIMITED
}
