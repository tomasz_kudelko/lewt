package pl.tk.lewt.services;

import java.util.List;

public interface CommonService<dtoType, entityType> {
    void update(dtoType dto);
    void delete(dtoType dto);
    void add(dtoType dto);
    List<dtoType> findAll();
}
