package pl.tk.lewt.services;

import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.User;

import java.util.List;

public interface UserService<UserDTO, User> extends CommonService<UserDTO, User> {
    List<CourseDTO> findUserCourses(UserDTO userDTO);
}
