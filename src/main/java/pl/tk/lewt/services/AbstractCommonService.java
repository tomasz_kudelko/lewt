package pl.tk.lewt.services;

import com.sun.istack.internal.Nullable;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.tk.lewt.dao.HibernateDao;
import pl.tk.lewt.entities.BaseEntity;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractCommonService<dtoType, entityType extends BaseEntity>
        implements CommonService<dtoType, entityType> {

    private Class<entityType> entityTypeClass;
    private Class<dtoType> dtoTypeClass;

    private Logger logger = Logger.getLogger(AbstractCommonService.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private HibernateDao<entityType> hibernateDao;

    @SuppressWarnings("unchecked")
    public AbstractCommonService() {
        dtoTypeClass = (Class<dtoType>) getGenericType(0);
        entityTypeClass = (Class<entityType>) getGenericType(1);
    }

    private Type getGenericType(int index) {
        final Type t = getClass().getGenericSuperclass();
        final ParameterizedType pt = (ParameterizedType) t;
        return pt.getActualTypeArguments()[index];
    }

    @Override
    public void update(dtoType dto) {
        final entityType entityToUpdate = modelMapper.map(dto, entityTypeClass);
        if (hibernateDao.exists(entityToUpdate)) {
            hibernateDao.update(entityToUpdate);
        } else {
            logger.error("Cannot update, given " + entityTypeClass.getName() + " doesn't exists: " + dto);
        }
    }

    @Override
    public void delete(dtoType dto) {
        final entityType entityToDelete = modelMapper.map(dto, entityTypeClass);
        if (hibernateDao.exists(entityToDelete)) {
            hibernateDao.delete(entityToDelete);
        } else {
            logger.error("Cannot delete, " + entityTypeClass.getName() + " doesn't exists: " + dto);
        }
    }

    @Override
    public void add(dtoType dto) {
        final entityType newEntity = modelMapper.map(dto, entityTypeClass);
        if (isAllowedToCreate(newEntity)) {
            hibernateDao.create(newEntity);
        } else {
            logger.error(entityTypeClass.getName() + " with given data already exists!");
        }
    }

    @Override
    public List<dtoType> findAll() {
        final List<entityType> all = hibernateDao.findAll();
        return all.stream()
                .map(entity -> modelMapper.map(entity, dtoTypeClass))
                .collect(Collectors.toList());
    }

    /**
     * Condition to override in descendants classes if necessary.
     */
    protected boolean isAllowedToCreate(entityType newEntity) {
        return true;
    }
}
