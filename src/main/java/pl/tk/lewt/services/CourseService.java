package pl.tk.lewt.services;

import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.Course;

import java.util.List;

public interface CourseService extends CommonService<CourseDTO, Course> {
}
