package pl.tk.lewt.services;

import pl.tk.lewt.dto.WordDTO;
import pl.tk.lewt.entities.Word;

public interface WordService extends CommonService<WordDTO, Word> {
}
