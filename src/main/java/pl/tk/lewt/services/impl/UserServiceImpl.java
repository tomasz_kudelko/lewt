package pl.tk.lewt.services.impl;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pl.tk.lewt.dao.UserDao;
import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.entities.User;
import pl.tk.lewt.services.AbstractCommonService;
import pl.tk.lewt.services.UserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends AbstractCommonService<UserDTO, User> implements UserService<UserDTO, User> {

    private Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CourseDTO> findUserCourses(final UserDTO userDTO) {
        final User user = userDao.findById(userDTO.getId());
        final Set<Course> userCourses = user.getCourses();
        return userCourses.stream()
                .map(course -> modelMapper.map(course, CourseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    protected boolean isAllowedToCreate(final User newUser) {
        final User userByLogin = userDao.findByLogin(newUser.getLogin());
        final User userByEmail = userDao.findByEmail(newUser.getEmail());

        return userByEmail == null && userByLogin == null;
    }
}
