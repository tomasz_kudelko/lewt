package pl.tk.lewt.services.impl;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tk.lewt.dao.WordDao;
import pl.tk.lewt.dto.WordDTO;
import pl.tk.lewt.entities.Word;
import pl.tk.lewt.services.AbstractCommonService;
import pl.tk.lewt.services.WordService;

@Service
public class WordServiceImpl extends AbstractCommonService<WordDTO, Word> implements WordService {

    private Logger logger = Logger.getLogger(WordServiceImpl.class);

    @Autowired
    private WordDao wordDao;

    @Override
    protected boolean isAllowedToCreate(final Word newWord) {
        final Word wordFromDb = wordDao.findByWordAndCourse(newWord);
        return wordFromDb == null;
    }
}
