package pl.tk.lewt.services.impl;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pl.tk.lewt.dao.CourseDao;
import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.services.AbstractCommonService;
import pl.tk.lewt.services.CourseService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl extends AbstractCommonService<CourseDTO, Course> implements CourseService {

    private final Logger logger = Logger.getLogger(CourseServiceImpl.class);

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private ModelMapper modelMapper;
}
