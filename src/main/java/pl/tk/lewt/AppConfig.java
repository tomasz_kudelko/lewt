package pl.tk.lewt;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.tk.lewt.utils.DtoEntityMapper;
import pl.tk.lewt.utils.JinqSource;

@Configuration
public class AppConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public JinqSource jinqSource() {
        return new JinqSource();
    }
}
