package pl.tk.lewt.dto;

import javax.persistence.Column;
import java.util.List;

public class WordDTO {

    private String originalWord;
    private List<String> translatedWords;
    private CourseDTO courseDTO;

    public WordDTO() {
    }

    public WordDTO(final String originalWord, final List<String> translatedWords, final CourseDTO courseDTO) {
        this.originalWord = originalWord;
        this.translatedWords = translatedWords;
        this.courseDTO = courseDTO;
    }

    public CourseDTO getCourseDTO() {
        return courseDTO;
    }

    public void setCourseDTO(CourseDTO courseDTO) {
        this.courseDTO = courseDTO;
    }

    public String getOriginalWord() {
        return originalWord;
    }

    public void setOriginalWord(String originalWord) {
        this.originalWord = originalWord;
    }

    public List<String> getTranslatedWords() {
        return translatedWords;
    }

    public void setTranslatedWords(List<String> translatedWords) {
        this.translatedWords = translatedWords;
    }
}
