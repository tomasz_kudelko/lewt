package pl.tk.lewt.dto;

import pl.tk.lewt.entities.Word;
import pl.tk.lewt.entities.enums.CourseAccessibility;

import java.util.HashSet;
import java.util.Set;

public class CourseDTO {

    private long id;
    private String name;
    private CourseAccessibility courseAccessibility;
    private Set<Word> wordSet;

    public CourseDTO() {
        wordSet = new HashSet<>();
    }

    public CourseDTO(final long id, final String name, final CourseAccessibility courseAccessibility, final Set<Word> wordSet) {
        this.id = id;
        this.name = name;
        this.courseAccessibility = courseAccessibility;
        this.wordSet = wordSet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseAccessibility getCourseAccessibility() {
        return courseAccessibility;
    }

    public void setCourseAccessibility(CourseAccessibility courseAccessibility) {
        this.courseAccessibility = courseAccessibility;
    }

    public Set<Word> getWordSet() {
        return wordSet;
    }

    public void setWordSet(Set<Word> wordSet) {
        this.wordSet = wordSet;
    }
}
