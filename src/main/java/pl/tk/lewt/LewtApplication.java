package pl.tk.lewt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LewtApplication {

	public static void main(String[] args) {
		SpringApplication.run(LewtApplication.class, args);
	}
}
