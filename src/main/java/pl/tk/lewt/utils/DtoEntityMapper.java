package pl.tk.lewt.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.tk.lewt.dto.CourseDTO;
import pl.tk.lewt.dto.WordDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.entities.Word;

@Component
public class DtoEntityMapper {

    private static ModelMapper modelMapper;

    @Autowired
    public DtoEntityMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public static WordDTO toDTO(final Word word) {
        return modelMapper.map(word, WordDTO.class);
    }

    public static Word toEntity(final WordDTO wordDTO) {
        return modelMapper.map(wordDTO, Word.class);
    }

    public static CourseDTO toDTO(final Course course) {
        return modelMapper.map(course, CourseDTO.class);
    }

    public static Course toEntity(final CourseDTO courseDTO) {
        return modelMapper.map(courseDTO, Course.class);
    }
}
