package pl.tk.lewt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.tk.lewt.entities.BaseEntity;

import java.util.List;

public interface HibernateDao<T extends BaseEntity> {

    T create(T entity);

    void delete(T entity);
    void deleteById(long id );

    T findById(long id);
    List<T> findByIds(List ids);
    List<T> findAll();

    T update(T entity);

    boolean exists(T entity);
}
