package pl.tk.lewt.dao.impl;

import com.sun.istack.internal.Nullable;
import org.springframework.stereotype.Repository;
import pl.tk.lewt.dao.AbstractHibernateDao;
import pl.tk.lewt.dao.WordDao;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.entities.Word;

@Repository
public class WordDaoImpl extends AbstractHibernateDao<Word> implements WordDao {

    @Override
    @Nullable
    public Word findByWordAndCourse(final Word newWord) {
        final String originalWord = newWord.getOriginalWord();
        final Course course = newWord.getCourse();
        return jinqStream().where(word -> word.getOriginalWord().equals(originalWord))
                .where(word -> word.getCourse().equals(course))
                .findOne().orElse(null);
    }

    @Override
    @Nullable
    public Word findByOriginalWord(final String originalWord) {
        return jinqStream().where(word -> word.getOriginalWord().equals(originalWord))
                .findOne().orElse(null);
    }
}
