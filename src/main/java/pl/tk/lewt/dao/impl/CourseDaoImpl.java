package pl.tk.lewt.dao.impl;

import org.springframework.stereotype.Repository;
import pl.tk.lewt.dao.AbstractHibernateDao;
import pl.tk.lewt.dao.CourseDao;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.Course;
import pl.tk.lewt.entities.enums.CourseAccessibility;

import java.util.List;

@Repository
public class CourseDaoImpl extends AbstractHibernateDao<Course> implements CourseDao {

}
