package pl.tk.lewt.dao.impl;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import pl.tk.lewt.dao.AbstractHibernateDao;
import pl.tk.lewt.dao.UserDao;
import pl.tk.lewt.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserDaoImpl extends AbstractHibernateDao<User> implements UserDao {

    public User findByLogin(@NotNull final String login) {
        return jinqStream().where(user -> user.getLogin().equals(login)).findFirst().orElse(null);
    }

    public User findByEmail(@NotNull final String email) {
        return jinqStream().where(user -> user.getEmail().equals(email)).findFirst().orElse(null);
    }
}
