package pl.tk.lewt.dao;

import org.springframework.stereotype.Repository;
import pl.tk.lewt.entities.User;

@Repository
public interface UserDao extends HibernateDao<User> {

    User findByLogin(String login);

    User findByEmail(String email);
}
