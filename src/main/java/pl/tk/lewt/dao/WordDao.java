package pl.tk.lewt.dao;

import pl.tk.lewt.dto.WordDTO;
import pl.tk.lewt.entities.Word;

public interface WordDao extends HibernateDao<Word> {
    Word findByWordAndCourse(Word newWord);
    Word findByOriginalWord(String originalWord);
}
