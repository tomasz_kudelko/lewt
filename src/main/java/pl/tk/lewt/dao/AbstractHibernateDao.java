package pl.tk.lewt.dao;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.tk.lewt.entities.BaseEntity;
import pl.tk.lewt.utils.JinqSource;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

@Repository
public abstract class AbstractHibernateDao<T extends BaseEntity> implements HibernateDao<T> {

    private Class<T> domainClass;

    @Autowired
    private JinqSource jinqSource;

    @PersistenceContext
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public AbstractHibernateDao() {
        final Type t = getClass().getGenericSuperclass();
        final ParameterizedType pt = (ParameterizedType) t;
        domainClass = (Class<T>) pt.getActualTypeArguments()[0];
    }

    @Override
    @NotNull
    public T create(@NotNull T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public void delete(@NotNull T entity) {
        entityManager.remove(entity);
    }

    @Override
    public void deleteById(long id) {
        final T entityById = findById(id);

        if (entityById != null) {
            entityManager.remove(entityById);
        } else {
            throw new IllegalArgumentException("Cannot find entity with id=" + id);
        }
    }

    @Override
    @Nullable
    public T findById(long id) {
        return entityManager.find(domainClass, id);
    }

    @Override
    @NotNull
    @SuppressWarnings("unchecked")
    public List<T> findByIds(@NotNull List ids) {
        return entityManager.createQuery("select * from " + domainClass.getName()
                + " where ID in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    @NotNull
    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery("from " + domainClass.getName()).getResultList();
    }

    @Override
    public T update(@NotNull T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean exists(@NotNull T entity) {
        final T result = findById(entity.getId());
        return result != null;
    }

    protected final JPAJinqStream<T> jinqStream() {
        return jinqSource.streamAll(entityManager, domainClass);
    }
}
