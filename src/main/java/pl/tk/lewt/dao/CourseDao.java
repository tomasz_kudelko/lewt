package pl.tk.lewt.dao;

import org.springframework.stereotype.Repository;
import pl.tk.lewt.dto.UserDTO;
import pl.tk.lewt.entities.Course;

import java.util.List;

@Repository
public interface CourseDao extends HibernateDao<Course> {

}
